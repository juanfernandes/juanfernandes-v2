---
layout: page
title: Now
permalink: "/now/"
date: 2023-09-20T00:00:00Z
class: now
intro:
  heading: Now
  subheading: What I'm currently up to
description: Read about what I'm currently up to
keywords: now, freelance website designer, frontend developer, reading, watching, work

---
### Personal
* Still no luck in buying a house - so may as well give up for a while as it can only happen if my old house sells.

### Projects
* I've rebuilt my ecommerce shop website and am nearly ready to properly launch it. Need to work out how to do marketing for it.

### Reading
* [The Outsider by Stephen King](https://bookwyrm.social/book/36200/s/the-outsider "The Outsider by Stephen King")
* [Company of One](https://bookwyrm.social/book/184714/s/company-of-one "Company of One")
* Always reading web design and business articles via my [Instapaper](https://www.instapaper.com/p/juanfernandes "Juan Fernandes on Instapaper")

### Watching
* [Community](https://www.themoviedb.org/tv/18347-community "Community")
  Follow the lives of a group of students at what is possibly the world’s worst community college in the fictional locale of Greendale, Colorado.

#### Paused
* [Black Knight](https://www.themoviedb.org/tv/137040 "Black Knight")
  In a dystopian 2071 devastated by air pollution, the survival of humanity depends on the Black Knights — and they’re far from your average delivery men.

#### Finished
* [Seinfeld (Rewatch)](https://www.themoviedb.org/tv/1400-seinfeld "Seinfeld")
* [Unstable](https://www.themoviedb.org/tv/219788-unstable "Unstable")
* [Welcome to Eden](https://www.themoviedb.org/tv/128010-bienvenidos-a-eden "Welcome to Eden")
