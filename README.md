[![Built with Grunt](http://cdn.gruntjs.com/builtwith.svg)](https://gruntjs.com/)
[![Powered by 11ty.io](https://img.shields.io/badge/powered%20by-11ty.io-%23000.svg)](https://11ty.io/)

---

# juanfernandes.uk

> Source files for my [personal website](https://juanfernandes.uk) built with Eleventy and hosted with Netlify.

## Getting Started

```bash
# Clone this repository:
git clone git@gitlab.com:juanfernandes/juanfernandes-v2.git

# Navigate to the directory
cd juanfernandes-v2

# Install dependencies
npm install

# Start local dev server
npx eleventy --serve

# Or build automatically when a template changes:
npx eleventy --watch

# Run Grunt
grunt dev

# Or build automatically when CSS changes:
grunt watch
```

## Deployments
[![Netlify Status](https://api.netlify.com/api/v1/badges/ad9cac69-cad5-4015-9a0e-98ceef6380da/deploy-status)](https://app.netlify.com/sites/juanfernandes/deploys)
